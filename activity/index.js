let posts = [];
let count = 1;

//add post data

document.querySelector("#form-add-post").addEventListener("submit", (e) => {
  e.preventDefault();
  posts.push({
    id: count,
    title: document.querySelector("#txt-title").value,
    body: document.querySelector("#txt-body").value,
  });
  count++; //increment movie post ids

  showPosts(posts);
  alert("Successfully added.");
});

//showPost function
const showPosts = (posts) => {
  let postEntries = "";

  posts.forEach((post) => {
    postEntries += `
      <div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>
        <p id="post-body-${post.id}">${post.body}</p>
        <button onclick="editPost('${post.id}')">Edit</button>
        <button onclick="deletePost('${post.id}')">Delete</button>
      </div>
    `;
  });

  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

//editing and updating a post

//will determine the id of the post that is selected
//editPost is the same as the onclick="editPost" so it is triggering the function when the it is clicked or "on click"
const editPost = (id) => {
  //retrieiving specific post you want to edit
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
};

//update post -- dont forget # in the query selector for id

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();

  for (let i = 0; i < posts.length; i++) {
    if (
      posts[i].id.toString() === document.querySelector("#txt-edit-id").value
    ) {
      posts[i].title = document.querySelector("#txt-edit-title").value;
      posts[i].body = document.querySelector("#txt-edit-body").value;

      showPosts(posts);
      alert("Successfully updated.");

      break;
    }
  }
});
console.log(posts);

//ACTIVITY: DELETE POST
const deletePost = (id) => {
  posts = posts.filter((post) => post.id != id);
  showPosts(posts);
};

console.log(posts);

// SOLUTION
// const deletePost = (id) => {
//   posts = posts.filter((post) => {
//     if (post.id.toString() !== id) {
//       //should use strict comparisons to avoid errors so need to use .toString()
//       return post;
//     }
//   });
//   document.querySelector(`#post=${id}`).remove(); //removes the element from the array itself
// };
