let posts = [];
let count = 1;

//add post data

document.querySelector("#form-add-post").addEventListener("submit", (e) => {
  e.preventDefault(); //prevents the page from loading after ever event or command I think bc the posts array restarts/empties every command because the page is reloading
  posts.push({
    id: count,
    title: document.querySelector("#txt-title").value,
    body: document.querySelector("#txt-body").value,
  });
  count++; //increment movie post ids

  showPosts(posts);
  alert("Successfully added.");
});

/* 
  Show posts 
*/

const showPosts = (posts) => {
  let postEntries = "";

  posts.forEach((post) => {
    postEntries += `
      <div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>
        <p id="post-body-${post.id}">${post.body}</p>
        <button onclick="editPost('${post.id}')">Edit</button>
        <button onclick="deletePost('${post.id}')">Delete</button>
      </div>
    `;
  });

  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

//editing and updating a post
/* 
  .innerHTML -  literal html markup
  .value - 
*/

//will determine the id of the post that is selected
//editPost is the same as the onclick="editPost" so it is triggering the function when the it is clicked or "on click"
const editPost = (id) => {
  //retrieiving specific post you want to edit
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
};

//update post -- don't forget # in the query selector for id

//toString = the id from the array is a number and we are comparing it the value of the txt edit id value kept in the id element which is a string
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();

  // id = 1 index 0
  // posts = 2 elements, 0, 1
  // posts.length = 2
  for (let i = 0; i < posts.length; i++) {
    // String 1 == Number 1
    // String 1 === Number 1

    if (
      posts[i].id.toString() === document.querySelector("#txt-edit-id").value
    ) {
      posts[i].title = document.querySelector("#txt-edit-title").value;
      posts[i].body = document.querySelector("#txt-edit-body").value;

      showPosts(posts);
      alert("Successfully updated.");

      break;
    }
  }
});
console.log(posts);

//delete post
const deletePost = (id) => {
  posts = posts.filter((post) => post.id != id);
  showPosts(posts);
};
